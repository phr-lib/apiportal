<?php

namespace Phr\Apiportal;

use Controllers\ApiController;
use Phr\Apiportal\AppException;
use Phr\Apiportal\Base\AppErrorResponse;
use Phr\Apiportal\Base\Uty\SettingsHandler;
use Phr\Apiportal\Settings\Authority;
use Phr\Apiportal\Settings\ServerSettings;
use Phr\Webapi\Settings;
use Phr\Sqlbridge\SqlSettings;
use Phr\Webapi\Base\Uty\SettingsException;
use Phr\Apiportal\Base\Uty\SettingsLock;
use Phr\Apiportal\Base\SettingsConfig;
use Phr\Apiportal\Settings\EngineSettings;

class App extends Base\AppPortalBase
{   
    /**
     * @access public 
     * 
     * @method engineSettings
     * @return string engine settings
     * 
     */
    public static function engineSettings(): string
    {
        $EngineSettings = new EngineSettings(
            self::$portalSettings->serverSettings,
            self::$portalSettings->authority
        );
        return $EngineSettings->json();
    }

    /**
     * @method configure
     * @var string path to config
     */
    public function configure( string $_path_to_config ): void
    {   
        try
        {   
            if(!file_exists( self::$ROOT . $_path_to_config )) throw new SettingsException("missing setting file");
            
            $Configuration = new SettingsConfig( self::$ROOT, $_path_to_config );
            
            self::$portalSettings = $Configuration->getPortalSettings();
            
        }
        catch( SettingsException $error)
        {
            throw new AppException( $error->getMessage() );
        }     
    }

   /**
     * @access public 
     * 
     * @static createSettingsModel
     * 
     * Creates appconfig.json configuration file model.
     * 
     */
    public static function createAppSettingsModel( string $_root ): void 
    {   
        $Handler = new SettingsHandler(
            new Settings\GeneralSettings(true, 'system'),
            new Settings\BaseSettings("","",""),
            new Settings\SecuritySettings(null, null,null),
            new Settings\DatabaseSettings('settings', new SqlSettings("","","","") ),
            new Settings\Authorisation("","",null),
            new ServerSettings("",443,""),
            new Authority("","","",null)
        );

        $AppConfig =  $Handler->toJson();

        file_put_contents( $_root . 'system/appconfig.json' ,$AppConfig );
    }
    public static function createLockFile( string $_root, string $_config_file_path )
    {   
        try
        {   
            $Settings = json_decode( file_get_contents( $_root . $_config_file_path ) );

            $SettingsLock = new SettingsLock( $_root, $Settings );

            $SettingsLock->createFile( $_root );
        }
        catch( SettingsException $error)
        {
            throw new AppException( $error->getMessage() );
            
        }
    }
    public function __construct( string $_ROOT )
    {
        parent::__construct( $_ROOT );

        self::includeAutoloader();  
    }
    
    public function useControllers()
    {
        ApiController::reroute();
    }

    private static function includeAutoloader()
    {
        require_once 'loaders/load.php';
    }

    public static function error( AppException $_exception )
    {
        echo AppErrorResponse::response( $_exception );
    }
}