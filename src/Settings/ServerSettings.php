<?php

namespace Phr\Apiportal\Settings;

class ServerSettings 
{   
    public string $host;

    public int $port;

    public bool $ssl;

    public function __construct( string $_host, int $_post, bool $_ssl )
    {
        $this->host = $_host;

        $this->port = $_post;

        $this->ssl = $_ssl;
    }
}




