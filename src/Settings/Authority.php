<?php

namespace Phr\Apiportal\Settings;

class Authority 
{
    public string $issuer;

    public string $clientId;

    public string|null $clientSecret;

    public string $redirectUrl;

    public function __construct( string $_issuer, string $_client_id, string $_redirect_url, string|null $_client_secret )
    {
        $this->issuer = $_issuer;

        $this->clientId = $_client_id;

        $this->redirectUrl = $_redirect_url;

        $this->clientSecret = $_client_secret;

    }

}