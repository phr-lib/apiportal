<?php

namespace Phr\Apiportal\Settings;

use Phr\Webapi\Settings\AppSettings;

class AppPortalSettings
{   
    public AppSettings $appSettings;

    public ServerSettings $serverSettings;

    public Authority $authority;

}