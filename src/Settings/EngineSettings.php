<?php

namespace Phr\Apiportal\Settings;

class EngineSettings 
{   
    public ServerSettings $serverSettings;

    public Authority $authority;

    public function __construct( ServerSettings $_server_settings, Authority $_authoroty)
    {
        $this->serverSettings = $_server_settings;

        $this->authority = $_authoroty;
    }

    public function json()
    {
        return json_encode( $this );
    }
}

