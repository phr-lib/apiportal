<?php

namespace Phr\Apiportal;

class AppException extends \Exception 
{   
    public bool $document;

    public function __construct( string $_message, int $_error_code, bool $_document = false )
    {
        $this->message = $_message;

        $this->code = $_error_code;

        $this->document = $_document;
    }
}