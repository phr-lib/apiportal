<?php

namespace Phr\Apiportal\Base;

use Phr\Webapi\Base\SettingsConfig as BaseClass;
use Phr\Apiportal\Settings\AppSettings;
use Phr\Apiportal\Settings\ServerSettings;
use Phr\Apiportal\Settings\Authority;
use Phr\Apiportal\Settings\AppPortalSettings;

class SettingsConfig extends BaseClass
{   
   /**
     * 
     * @access public
     * @method getPortalSettings
     * @return AppPortalSettings
     * 
     */
    public function getPortalSettings(): AppPortalSettings
    {   
        return self::$portalSettings;
    } 

    /**
     * @access protected 
     * @var AppSettings
     */
    protected static AppPortalSettings $portalSettings;

    /// CONSTRUCT ***

    public function __construct( string $ROOT, string $_config_path )
    {
        parent::__construct( $ROOT, $_config_path);

        $PortalSettings = new AppPortalSettings;

        $PortalSettings->appSettings = self::$AppSettings;
        
        $Settings = json_decode( file_get_contents( $ROOT . $_config_path ) );

        $PortalSettings->serverSettings = $this->extractServerSettings( $Settings );

        $PortalSettings->authority = $this->extractAuthorotySettings( $Settings );

        self::$portalSettings = $PortalSettings;
    }

    /**
     * @access private
     * 
     * @method extractServerSettings
     * @return ServerSettings
     * @var settings
     */
    private function extractServerSettings( $_settings ): ServerSettings
    {   
        return new ServerSettings(
            $_settings->serverSettings->host,
            $_settings->serverSettings->port,
            $_settings->serverSettings->ssl
        );
    }

    /**
     * @method extractAuthorotySettings
     * @return Authority
     * @var settings
     */
    private function extractAuthorotySettings( $_settings ): Authority
    {
        return new Authority(
            $_settings->authority->issuer,
            $_settings->authority->clientId,
            $_settings->authority->redirectUrl,
            $_settings->authority->clientSecret
        );
    }
}