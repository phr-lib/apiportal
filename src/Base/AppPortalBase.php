<?php

namespace Phr\Apiportal\Base;

use Phr\Webapi\Base\WebapiBase;
use Phr\Apiportal\Settings\AppPortalSettings;

abstract class AppPortalBase extends WebapiBase
{
    public static AppPortalSettings $portalSettings;
}