<?php

namespace Phr\Apiportal\Base;

use Phr\Apiportal\AppException;
use Phr\Webapi\Api;
use Phr\Webapi\WebApiException;

class AppErrorResponse
{
    public static function response( AppException $_exception )
    {   
        if( $_exception->document == true)
        {
            self::documentResponse( $_exception );
        }
        else 
        {
            return Api::apiError( new WebApiException( $_exception->getMessage(), $_exception->getCode() ) );
        }        
    }

    private static function documentResponse( AppException $_exception  )
    {
        echo 'ERROR::'.$_exception->getMessage();
    }
}