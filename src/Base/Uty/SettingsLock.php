<?php

namespace Phr\Apiportal\Base\Uty;

use Phr\Webapi\Base\Uty\SettingsLock as BaseClass;
use Phr\Apiportal\Settings\ServerSettings;
use Phr\Apiportal\Settings\Authority;



class SettingsLock extends BaseClass
{   
    public ServerSettings $serverSettings;

    public Authority $authoritySettings;

    public function __construct( string $ROOT, $_settings )
    {   
        parent::__construct( $ROOT, $_settings );

        $this->serverSettings = new ServerSettings(
            $_settings->serverSettings->host,
            $_settings->serverSettings->port,
            $_settings->serverSettings->ssl
        );

        $this->authoritySettings = new Authority(
            $_settings->authority->issuer,
            $_settings->authority->clientId,
            $_settings->authority->redirectUrl,
            $_settings->authority->clientSecret
        );
    }
}