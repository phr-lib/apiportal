<?php

namespace Phr\Apiportal\Base\Uty;


use Phr\Webapi\Base\Uty\SettingsHandler as WebApiHandler;
use Phr\Apiportal\Settings\Authority;
use Phr\Apiportal\Settings\ServerSettings;
use Phr\Webapi\Settings\GeneralSettings;
use Phr\Webapi\Settings\BaseSettings;
use Phr\Webapi\Settings\SecuritySettings;
use Phr\Webapi\Settings\DatabaseSettings;
use Phr\Webapi\Settings\Authorisation;

class SettingsHandler extends WebApiHandler
{
    public function __construct(
        GeneralSettings $_general_settings,
        BaseSettings $_base_settings,
        SecuritySettings $_security_settings,
        DatabaseSettings $_database_settings,
        Authorisation $_authorisation_settings,
        ServerSettings $_server_settings,
        Authority $_authoroty
    )
    {   
        parent::__construct(
            $_general_settings,
            $_base_settings,
            $_security_settings,
            $_database_settings,
            $_authorisation_settings
        );

        $this->serverSettings = $_server_settings;

        $this->authority = $_authoroty;

    }
}